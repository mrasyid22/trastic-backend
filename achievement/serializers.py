from pyexpat import model
from rest_framework import serializers
from .models import Achievement, Stage1Achievement, Stage2Achievement, Stage3Achievement, Stage4Achievement

class Stage1Serializer(serializers.ModelSerializer):
    class Meta:
        model = Stage1Achievement
        fields = ["id","is_achieved", "is_claimed"]

class Stage2Serializer(serializers.ModelSerializer):
    class Meta:
        model = Stage2Achievement
        fields = ["id", "is_achieved", "is_claimed"]

class Stage3Serializer(serializers.ModelSerializer):
    class Meta:
        model = Stage3Achievement
        fields = ["id", "is_achieved", "is_claimed"]

class Stage4Serializer(serializers.ModelSerializer):
    class Meta:
        model = Stage4Achievement
        fields = ["id", "is_achieved", "is_claimed"]
        
class AchievementsSerializer(serializers.ModelSerializer):
    stage1 = Stage1Serializer()
    stage2 = Stage2Serializer()
    stage3 = Stage3Serializer()
    stage4 = Stage4Serializer()
    
    class Meta:
        model = Achievement
        fields = ['id', "stage1", "stage2", "stage3", "stage4"]