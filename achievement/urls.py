from django.urls import path, re_path
from .views import ListAchievementsView, ConfirmStage1AchievementView, ConfirmStage2AchievementView, ConfirmStage3AchievementView, ConfirmStage4AchievementView

app_name = "achievement"
urlpatterns = [
    path('achievements/', ListAchievementsView.as_view(),name='achievements'),
    path('claim/stage1/',ConfirmStage1AchievementView.as_view(), name='confirmation stage1'),
    path('claim/stage2/',ConfirmStage2AchievementView.as_view(), name='confirmation stage2'),
    path('claim/stage3/',ConfirmStage3AchievementView.as_view(), name='confirmation stage3'),
    path('claim/stage4/',ConfirmStage4AchievementView.as_view(), name='confirmation stage4'),
]