from django.urls import path, re_path
from .views import LocationListView
app_name = "location"
urlpatterns = [
    path('list-location/', LocationListView.as_view(),name='list-location')
]