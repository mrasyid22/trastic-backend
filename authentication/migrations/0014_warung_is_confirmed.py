# Generated by Django 3.1.7 on 2022-06-01 21:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0013_auto_20220522_2330'),
    ]

    operations = [
        migrations.AddField(
            model_name='warung',
            name='is_confirmed',
            field=models.BooleanField(default=False),
        ),
    ]
