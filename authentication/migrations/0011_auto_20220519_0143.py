# Generated by Django 3.1.7 on 2022-05-19 01:43

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('authentication', '0010_auto_20220513_1757'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='alamat',
            options={'ordering': ['-is_main']},
        ),
    ]
