from rest_framework import permissions

class IsPengguna(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_pengguna

class IsAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_superuser

class IsWarung(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.is_warung