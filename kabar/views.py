from django.shortcuts import render
from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView, CreateAPIView
from .models import Kabar
from .serializers import KabarSerializer
from .permissions import IsAdmin
from rest_framework import generics, status, views, permissions, viewsets

class ListKabarView(ListAPIView):
    serializer_class = KabarSerializer

    def get_queryset(self):
        return Kabar.objects.all()
    
class CreateKabarView(CreateAPIView):
    serializer_class = KabarSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
