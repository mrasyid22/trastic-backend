from django.urls import path, re_path
from .views import ExchangePenjemputanDetailView, ExchangeSatuanPendingListView, KabarDetailView, KabarListView, PenjemputanWarungListView, UpdateExchangePenjemputanView, UpdateExchangeSatuanPendingView, UpdateKabarView, UpdatePenjemputanWarungView, UpdateWithdrawPointsView, WarungPendingListView, WarungStatisticsView, WarungListView, WarungDetailView, UpdateWarungView, PenggunaStatisticsView, PenggunaListView, PenggunaDetailView, UpdatePenggunaView,ExchangePaketanPendingListView, UpdateExchangePaketanPendingView, ExchangePaketanPendingDetailView, LocationCreateView, UpdateLocationView, ExchangePenjemputanListView, WithdrawPointsDetailView, WithdrawPointsListView


app_name = "admin-bo"
urlpatterns = [
    path('all-warung-statistics/', WarungStatisticsView.as_view(), name='all-warung-statistics'),
    path('warung-list/', WarungListView.as_view(), name='list-warung'),
    path('pengguna-list/', PenggunaListView.as_view(), name='list-pengguna'),
    path('warung-pending/', WarungPendingListView.as_view(), name='list-warung'),
    re_path('^warung/(?P<custom_id>\w+)/$',WarungDetailView.as_view(), name='warung_detail'),
    re_path('^pengguna/(?P<user>\w+)/$',PenggunaDetailView.as_view(), name='pengguna_detail'),
    re_path('^exchange-paketan/(?P<custom_id>\w+)/$',ExchangePaketanPendingDetailView.as_view(), name='exchange_paketan_detail'),
    re_path('^exchange-penjemputan/(?P<custom_id>\w+)/$',ExchangePenjemputanDetailView.as_view(), name='exchange_penjemputan_detail'),
    re_path('^warung/update/(?P<custom_id>\w+)/$',UpdateWarungView.as_view(), name='warung_update'),
    re_path('^exchange-paketan/update/(?P<custom_id>\w+)/$',UpdateExchangePaketanPendingView.as_view(), name='exchange_paketan_update'),
    re_path('^pengguna/update/(?P<user>\w+)/$',UpdatePenggunaView.as_view(), name='pengguna_update'),
    path('all-pengguna-statistics/', PenggunaStatisticsView.as_view(), name='all-pengguna-statistics'),
    path('exchange-paketan/', ExchangePaketanPendingListView.as_view(), name='list-exchange-paketan-pending'),
    path('location/create/', LocationCreateView.as_view(), name='create-location'),
    re_path('^location/update/(?P<warung>\w+)/$',UpdateLocationView.as_view(), name='location-update'),
    path('exchange-penjemputan/', ExchangePenjemputanListView.as_view(), name='list-exchange-penjemputan'),
    re_path('^exchange-penjemputan/update/(?P<custom_id>\w+)/$',UpdateExchangePenjemputanView.as_view(), name='exchange-penjemputan-update'),
    path('withdraw-points/', WithdrawPointsListView.as_view(), name='list-withdraw-points'),
    re_path('^withdraw-points/update/(?P<custom_id>\w+)/$',UpdateWithdrawPointsView.as_view(), name='withdraw-points-update'),re_path('^withdraw-points/(?P<custom_id>\w+)/$',WithdrawPointsDetailView.as_view(), name='withdraw-points-detail'),
    path('kabar/', KabarListView.as_view(), name='list-kabar'),
    re_path('^kabar/update/(?P<id>\w+)/$',UpdateKabarView.as_view(), name='kabar-update'),
    re_path('^kabar/(?P<id>\w+)/$',KabarDetailView.as_view(), name='kabar-detail'),
    path('penjemputan-warung/', PenjemputanWarungListView.as_view(), name='list-exchange-penjemputan'),
    re_path('^penjemputan-warung/update/(?P<custom_id>\w+)/$',UpdatePenjemputanWarungView.as_view(), name='penjemputan-warung-update'),
    path('exchange-satuan/', ExchangeSatuanPendingListView.as_view(), name='list-exchange-satuan-pending'),
    re_path('^exchange-satuan/update/(?P<custom_id>\w+)/$',UpdateExchangeSatuanPendingView.as_view(), name='exchange_satuan_update'),
]