from urllib import request, response
from django.shortcuts import render
from kabar.models import Kabar
from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView, CreateAPIView,  RetrieveUpdateDestroyAPIView, ListCreateAPIView
from authentication.models import Alamat, User, Pengguna, Warung
from achievement.models import Achievement, Stage1Achievement, Stage2Achievement, Stage3Achievement, Stage4Achievement
from .serializers import ExchangePenjemputanSerializer, ExchangeSatuanSerializer, KabarSerializer, PenggunaSerializer, PenjemputanWarungSerializer, WarungSerializer, ExchangePaketanSerializer, LocationSerializer, WithdrawSerializer
from exchange.models import ExchangePenjemputan, PenjemputanWarung, Transaction, ExchangeSatuan, ExchangePaketan
from location.models import Location
from exchange.permissions import IsAdmin, IsPengguna, IsWarung
from points.models import TransferPoints, WithdrawPoints
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import generics, status, views, permissions, viewsets
from django.db.models import Sum
from rest_framework.pagination import PageNumberPagination, LimitOffsetPagination
from django.db.models import Q
import datetime

class WarungStatisticsView(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    def get(self, request):
        #Jumlah Warung
        jumlah_warung = len(Warung.objects.all())
        total_jumlah_penukaran = self.get_jumlah_penukaran()
        top_3 = self.get_top_3_warung()
        response = {
            'jumlah_warung': jumlah_warung,
            'jumlah_kemasan_pending': self.get_jumlah_total_kemasan_pending(),
            'jumlah_total_penukaran_satuan_month':total_jumlah_penukaran['satuan_month'],
            'jumlah_total_penukaran_satuan_week': total_jumlah_penukaran['satuan_week'],
            'jumlah_total_penukaran_paketan_month':total_jumlah_penukaran['paketan_month'] ,
            'jumlah_total_penukaran_paketan_week':total_jumlah_penukaran['paketan_week'],
            'top_3_warung': top_3,
            'jumlah_akun_warung_belum_dikonfirmasi':self.get_warung_unconfirmed(),
            'jumlah_penjemputan_warung_pending': self.get_penjemputan_pending(),
            'total_poin_semua_warung': self.get_total_poin_warung(),
            'total_poin_withdraw_pending': self.get_total_withdraw_pending()
        }
        return Response(response)

    def get_jumlah_total_kemasan_pending(self):
        exchange_paketan_pending = ExchangePaketan.objects.filter(is_confirmed=False, is_picked_up=False)
        jumlah_kemasan_pending_paketan = 0
        if(exchange_paketan_pending):
            jumlah_kemasan_pending_paketan = ExchangePaketan.objects.filter(is_confirmed=False, is_picked_up=False).aggregate(Sum('total_kemasan'))['total_kemasan__sum']

        exchange_satuan_pending = ExchangeSatuan.objects.filter(is_picked_up=False)
        jumlah_kemasan_pending_satuan = 0
        if(exchange_satuan_pending):
            jumlah_kemasan_pending_satuan = int(ExchangeSatuan.objects.filter(is_picked_up=False).aggregate(Sum('total_kemasan'))['total_kemasan__sum'])
            
        return jumlah_kemasan_pending_paketan + jumlah_kemasan_pending_satuan
    
    def get_jumlah_penukaran(self):
        date = datetime.datetime.today()
        week = date.strftime("%V")
        month = date.strftime("%m")
        
        satuan_week = ExchangeSatuan.objects.filter(date__week=week).aggregate(Sum('total_kemasan'))['total_kemasan__sum']
        paketan_week = ExchangePaketan.objects.filter(date__week=week).aggregate(Sum('total_kemasan'))['total_kemasan__sum']
        
        satuan_month = ExchangeSatuan.objects.filter(date__month=month).aggregate(Sum('total_kemasan'))['total_kemasan__sum']
        paketan_month = ExchangePaketan.objects.filter(date__month=month).aggregate(Sum('total_kemasan'))['total_kemasan__sum']
        
        response = {
            'satuan_week': satuan_week,
            'paketan_week': paketan_week,
            'satuan_month':satuan_month,
            'paketan_month': paketan_month
        }
        
        return response

    def get_top_3_warung(self):
        warungs = Warung.objects.all()
        list = []

        for warung in warungs:
            satuan = ExchangeSatuan.objects.filter(warung=warung)
            paketan = ExchangePaketan.objects.filter(warung=warung)

            list.append(len(satuan) + len(paketan))
            
        list,warungs = zip(*sorted(zip(list,warungs), reverse=True,key=lambda x: x[0]))
        
        response = {}
        i = 0
        while i<3:
            try:
                response["{}".format(i+1)] = [warungs[i].name, list[i]]
                i+=1
            except:
                response["{}".format(i+1)] = [None, None]
                i+=1
            
        return response

    def get_warung_unconfirmed(self):
        warungs = Warung.objects.filter(is_confirmed =False)
        
        return(len(warungs))
    
    def get_penjemputan_pending(self):
        penjemputan_warung = PenjemputanWarung.objects.filter(status="Menunggu Konfirmasi")
        
        return(len(penjemputan_warung))
    
    def get_total_poin_warung(self):
        return (Warung.objects.all().aggregate(Sum('poin'))['poin__sum'])

        
    def get_total_withdraw_pending(self):
        return (WithdrawPoints.objects.filter(pengguna=None, is_confirmed=False).aggregate(Sum('pending_poin'))['pending_poin__sum'])
    
class Pagination(LimitOffsetPagination):
    max_limit = 10
    default_limit = 10
    limit_query_param = 'limit'
    offset_query_param = 'offset'

class WarungListView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    serializer_class = WarungSerializer
    pagination_class = Pagination
    
    def get_queryset(self):
        query = self.request.GET.get("q", None)
        warung = Warung.objects.all()
        if query is not None:
            warung = warung.filter(
                Q(custom_id__icontains=query) |
                Q(name__icontains=query) |
                Q(phone__icontains=query) |
                Q(city__icontains=query)
            )
            return warung
        else:
            return warung

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            serializer_data = serializer.data
            sorted_serializer_data = sorted(serializer_data, key=lambda x: x['custom_id'])
            return self.get_paginated_response(sorted_serializer_data)

        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['custom_id'])
        return Response(sorted_serializer_data)
    
class WarungDetailView(RetrieveAPIView):
    serializer_class = WarungSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    def get_object(self):
        custom_id = self.kwargs['custom_id']

        try:
            warung = Warung.objects.get(custom_id=custom_id)
            return warung
        
        except:
            return Response({'message': 'Warung tidak ditemukan'}, status=status.HTTP_400_BAD_REQUEST)

class UpdateWarungView(RetrieveUpdateDestroyAPIView):
    serializer_class = WarungSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    lookup_field = "custom_id"
    queryset = Warung.objects.all()
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if serializer.is_valid():
            serializer_data = serializer.save()
            return Response({'message': "Detail Warung Berhasil Diupdate"}, status=status.HTTP_200_OK)
        else:
            return Response({'message': "Detail Warung Gagal Diupdate"}, status=status.HTTP_400_BAD_REQUEST)


    

class WarungPendingListView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    serializer_class = WarungSerializer
    pagination_class = Pagination

    def get_queryset(self):
        warung = Warung.objects.filter(is_confirmed=False)
        query = self.request.GET.get("q", None)
        if query is not None:
            warung = warung.filter(
                Q(custom_id__icontains=query) |
                Q(name__icontains=query) |
                Q(phone__icontains=query) |
                Q(city__icontains=query)
            )
            return warung
        else:
            return warung

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            serializer_data = serializer.data
            sorted_serializer_data = sorted(serializer_data, key=lambda x: x['name'])
            return self.get_paginated_response(sorted_serializer_data)

        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['name'])
        return Response(sorted_serializer_data)
    


class PenggunaStatisticsView(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    def get(self, request):

        total_stages = self.get_total_pengguna_on_each_stage()
        response = {
            'jumlah_pengguna': len(Pengguna.objects.all()),
            'jumlah_total_poin_beredar': self.get_total_poin_beredar(),
            'top_3_pengguna': self.get_top_3_pengguna(),
            'total_poin_withdraw_pending': self.get_total_withdraw_pending(),
            'stage':[{
                'total_pengguna_stage_0': total_stages['stage0'],
                'total_pengguna_stage_1': total_stages['stage1'],
                'total_pengguna_stage_2': total_stages['stage2'],
                'total_pengguna_stage_3': total_stages['stage3'],
                'total_pengguna_stage_4': total_stages['stage4'],
            }]
        }
        return Response(response)

    def get_top_3_pengguna(self):
        penggunas = Pengguna.objects.all()
        list = []

        for pengguna in penggunas:
            satuan = ExchangeSatuan.objects.filter(pengguna=pengguna)
            paketan = ExchangePaketan.objects.filter(pengguna=pengguna)

            list.append(len(satuan) + len(paketan))
            
        list,penggunas = zip(*sorted(zip(list,penggunas), reverse=True,key=lambda x: x[0]))
        
        response = {}
        i = 0
        while i<5:
            try:
                response["{}".format(i+1)] = [penggunas[i].name, list[i]]
                i+=1
            except:
                response["{}".format(i+1)] = [None, None]
                i+=1
            
        return response
    
    def get_total_poin_beredar(self):
        return (Pengguna.objects.all().aggregate(Sum('poin'))['poin__sum'])

        
    def get_total_withdraw_pending(self):
        return (WithdrawPoints.objects.filter(warung=None, is_confirmed=False).aggregate(Sum('pending_poin'))['pending_poin__sum'])
    
    def get_total_pengguna_on_each_stage(self):
        total_stage_0= len(Pengguna.objects.filter(pencapaian="stage0"))
        total_stage_1= len(Stage1Achievement.objects.filter(is_achieved=True))
        total_stage_2= len(Stage2Achievement.objects.filter(is_achieved=True))
        total_stage_3= len(Stage3Achievement.objects.filter(is_achieved=True))
        total_stage_4= len(Stage4Achievement.objects.filter(is_achieved=True))
        
        response = {
            'stage0': total_stage_0,
            'stage1': total_stage_1,
            'stage2': total_stage_2,
            'stage3': total_stage_3,
            'stage4': total_stage_4,
        }
        
        return response


class PenggunaListView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    serializer_class = PenggunaSerializer
    pagination_class = Pagination
    
    def get_queryset(self):
        query = self.request.GET.get("q", None)
        pengguna = Pengguna.objects.all()
        if query is not None:
            pengguna = pengguna.filter(
                Q(name__icontains=query) |
                Q(phone__icontains=query) |
                Q(city__icontains=query)
            )
            return pengguna
        else:
            return pengguna

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            serializer_data = serializer.data
            sorted_serializer_data = sorted(serializer_data, key=lambda x: x['user'])
            return self.get_paginated_response(sorted_serializer_data)

        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['user'])
        return Response(sorted_serializer_data)

class PenggunaDetailView(RetrieveAPIView):
    serializer_class = PenggunaSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    def get_object(self):
        user = self.kwargs['user']
        try:
            pengguna = Pengguna.objects.get(user=user)
            return pengguna
        
        except:
            return Response({'message': 'Pengguna tidak ditemukan'}, status=status.HTTP_400_BAD_REQUEST)
        

class UpdatePenggunaView(RetrieveUpdateDestroyAPIView):
    serializer_class = PenggunaSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    lookup_field = "user"
    queryset = Pengguna.objects.all()
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if serializer.is_valid():
            serializer_data = serializer.save()
            return Response({'message': "Detail Pengguna Berhasil Diupdate"}, status=status.HTTP_200_OK)
        else:
            return Response({'message': "Detail Pengguna Gagal Diupdate"}, status=status.HTTP_400_BAD_REQUEST)
        

class ExchangePaketanPendingListView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    serializer_class = ExchangePaketanSerializer
    pagination_class = Pagination
    
    def get_queryset(self):
        query = self.request.GET.get("q", None)
        custom_id = self.request.GET.get("custom_id", None)
        if custom_id is not None:
            exchange = ExchangePaketan.objects.filter(is_confirmed=False).filter(penjemputan_warung=custom_id)
        else:
            exchange = ExchangePaketan.objects.filter(is_confirmed=False)
        if query is not None:
            exchange = exchange.filter(
                Q(custom_id__icontains=query)
            )
            return exchange
        else:
            return exchange

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            serializer_data = serializer.data
            sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'])
            return self.get_paginated_response(sorted_serializer_data)

        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'])
        return Response(sorted_serializer_data)
    
class ExchangePaketanPendingDetailView(RetrieveAPIView):
    serializer_class = ExchangePaketanSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    def get_object(self):
        custom_id = self.kwargs['custom_id']

        try:
            exchange_paketan = ExchangePaketan.objects.get(custom_id=custom_id)
            return exchange_paketan
        
        except:
            return Response({'message': 'Exchange Paketan tidak ditemukan'}, status=status.HTTP_400_BAD_REQUEST)

class UpdateExchangePaketanPendingView(RetrieveUpdateDestroyAPIView):
    serializer_class = ExchangePaketanSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    lookup_field = "custom_id"
    queryset = ExchangePaketan.objects.all()
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if serializer.is_valid():
            serializer_data = serializer.save()
            return Response({'message': "Detail Exchange Paketan Berhasil Diupdate"}, status=status.HTTP_200_OK)
        else:
            return Response({'message': "Detail Exchange Paketan Gagal Diupdate"}, status=status.HTTP_400_BAD_REQUEST)
        
class LocationCreateView(CreateAPIView):
    serializer_class = LocationSerializer
    permission_classes = (permissions.IsAuthenticated,IsAdmin)
    
    def perform_create(self, serializer):
        id_warung = self.request.data['warung']
        alamat = Location.objects.filter(warung=id_warung)
        if len(alamat) == 0:
            serializer_data =  serializer.save()
            return Response(serializer_data, status=status.HTTP_201_CREATED)
        elif len(alamat) >= 1:
            return Response({
                'message': "Warung dengan id {}, sudah memiliki location.".format(id_warung)
            }, status=status.HTTP_208_ALREADY_REPORTED)
            
class UpdateLocationView(RetrieveUpdateDestroyAPIView):
    serializer_class = LocationSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    lookup_field = "warung"
    queryset = Location.objects.all()
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if serializer.is_valid():
            serializer_data = serializer.save()
            return Response({'message': "Detail Location Berhasil Diupdate"}, status=status.HTTP_200_OK)
        else:
            return Response({'message': "Detail Location Gagal Diupdate"}, status=status.HTTP_400_BAD_REQUEST)
        
class ExchangePenjemputanListView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    serializer_class = ExchangePenjemputanSerializer
    
    pagination_class = Pagination
    
    def get_queryset(self):
        query = self.request.GET.get("q", None)
        exchange = ExchangePenjemputan.objects.filter(is_confirmed=False)
        if query is not None:
            exchange = exchange.filter(
                Q(custom_id__icontains=query)
            )
            return exchange
        else:
            return exchange

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            serializer_data = serializer.data
            sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'])
            return self.get_paginated_response(sorted_serializer_data)

        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'])
        return Response(sorted_serializer_data)

class ExchangePenjemputanDetailView(RetrieveAPIView):
    serializer_class = ExchangePenjemputanSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    def get_object(self):
        custom_id = self.kwargs['custom_id']

        try:
            exchange_penjemputan = ExchangePenjemputan.objects.get(custom_id=custom_id)
            return exchange_penjemputan
        
        except:
            return Response({'message': 'Exchange Penjemputantidak ditemukan'}, status=status.HTTP_400_BAD_REQUEST)

class UpdateExchangePenjemputanView(RetrieveUpdateDestroyAPIView):
    serializer_class = ExchangePenjemputanSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    lookup_field = "custom_id"
    queryset = ExchangePenjemputan.objects.all()
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if serializer.is_valid():
            serializer_data = serializer.save()
            return Response({'message': "Detail Exchange Penjemputan Berhasil Diupdate"}, status=status.HTTP_200_OK)
        else:
            return Response({'message': "Detail Exchange Penjemputan Gagal Diupdate"}, status=status.HTTP_400_BAD_REQUEST)
        
class WithdrawPointsListView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    serializer_class = WithdrawSerializer
    
    pagination_class = Pagination
    
    def get_queryset(self):
        query = self.request.GET.get("q", None)
        withdraw = WithdrawPoints.objects.filter(is_confirmed=False)
        if query is not None:
            withdraw = withdraw.filter(
                Q(custom_id__icontains=query) |
                Q(phone__icontains=query) |
                Q(method__icontains=query)
            )
            return withdraw
        else:
            return withdraw

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            serializer_data = serializer.data
            sorted_serializer_data = sorted(serializer_data, key=lambda x: x['custom_id'])
            return self.get_paginated_response(sorted_serializer_data)

        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['custom_id'])
        return Response(sorted_serializer_data)
    
class WithdrawPointsDetailView(RetrieveAPIView):
    serializer_class = WithdrawSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    def get_object(self):
        custom_id = self.kwargs['custom_id']

        try:
            withdraw = WithdrawPoints.objects.get(custom_id=custom_id)
            return withdraw
        
        except:
            return Response({'message': 'Withdraw Points tidak ditemukan'}, status=status.HTTP_400_BAD_REQUEST)
        
class UpdateWithdrawPointsView(RetrieveUpdateDestroyAPIView):
    serializer_class = WithdrawSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    lookup_field = "custom_id"
    queryset = WithdrawPoints.objects.all()
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if serializer.is_valid():
            serializer_data = serializer.save()
            return Response({'message': "Detail Exchange Penjemputan Berhasil Diupdate"}, status=status.HTTP_200_OK)
        else:
            return Response({'message': "Detail Exchange Penjemputan Gagal Diupdate"}, status=status.HTTP_400_BAD_REQUEST)

class KabarListView(ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    serializer_class = KabarSerializer
    
    pagination_class = Pagination
    
    def get_queryset(self):
        query = self.request.GET.get("q", None)
        kabar = Kabar.objects.all()
        if query is not None:
            kabar = kabar.filter(
                Q(title__icontains=query) |
                Q(text__icontains=query)
            )
            return kabar
        else:
            return kabar

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            serializer_data = serializer.data
            sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'])
            return self.get_paginated_response(sorted_serializer_data)

        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'])
        return Response(sorted_serializer_data)
    
class KabarDetailView(RetrieveAPIView):
    serializer_class = KabarSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    def get_object(self):
        id = self.kwargs['id']

        try:
            kabar = Kabar.objects.get(id=id)
            return kabar
        
        except:
            return Response({'message': 'Detail Kabar tidak ditemukan'}, status=status.HTTP_400_BAD_REQUEST)
        
class UpdateKabarView(RetrieveUpdateDestroyAPIView):
    serializer_class = KabarSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    lookup_field = "id"
    queryset = Kabar.objects.all()
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if serializer.is_valid():
            serializer_data = serializer.save()
            return Response({'message': "Detail Kabar Berhasil Diupdate"}, status=status.HTTP_200_OK)
        else:
            return Response({'message': "Detail Kabar Gagal Diupdate"}, status=status.HTTP_400_BAD_REQUEST)

class UpdatePenjemputanWarungView(RetrieveUpdateDestroyAPIView):
    serializer_class = PenjemputanWarungSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    lookup_field = "custom_id"
    queryset = PenjemputanWarung.objects.all()
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if serializer.is_valid():
            serializer_data = serializer.save()

            if serializer_data.status == "Menunggu Penjemputan":
                return Response({'message': "Segera lakukan penjemputan terhadap warung terkait"}, status=status.HTTP_200_OK)
            elif serializer_data.status == "Batal":
                return Response({'message': "Penjemputan berhasil dibatalkan"}, status=status.HTTP_200_OK)
            elif serializer_data.status == "Selesai":
                satuans = ExchangeSatuan.objects.filter(penjemputan_warung=instance).filter(is_picked_up=False, is_confirmed=True)
                paketans = ExchangePaketan.objects.filter(penjemputan_warung=instance).filter(is_picked_up=False, is_confirmed=False)
                
                for satuan in satuans:
                    satuan.is_picked_up=True
                    satuan.save()
                for paketan in paketans:
                    paketan.is_picked_up=True
                    paketan.is_confirmed = True
                    paketan.save()
                return Response({'message': "Penjemputan berhasil dikonfirmasi dan telah selesai dilakukan, poin warung telah ditambahkan."}, status=status.HTTP_200_OK)
        else:
            return Response({'message': "Detail Penjemputan Warung Gagal Diupdate"}, status=status.HTTP_400_BAD_REQUEST)
        

class PenjemputanWarungListView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    serializer_class = PenjemputanWarungSerializer
    
    pagination_class = Pagination
    
    def get_queryset(self):
        query = self.request.GET.get("q", None)
        penjemputan = PenjemputanWarung.objects.all()
        if query is not None:
            penjemputan = penjemputan.filter(
                Q(custom_id__icontains=query) |
                Q(status__icontains=query)
            )
            return penjemputan
        else:
            return penjemputan

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            serializer_data = serializer.data
            sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'])
            return self.get_paginated_response(sorted_serializer_data)

        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'])
        return Response(sorted_serializer_data)

class ExchangeSatuanPendingListView(ListAPIView):
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    serializer_class = ExchangeSatuanSerializer
    pagination_class = Pagination
    
    def get_queryset(self):
        query = self.request.GET.get("q", None)
        custom_id = self.request.GET.get("custom_id", None)
        if custom_id is not None:
            exchange = ExchangeSatuan.objects.filter(is_picked_up=False).filter(penjemputan_warung=custom_id)
        else:
            exchange = ExchangeSatuan.objects.filter(is_picked_up=False)
        if query is not None:
            exchange = exchange.filter(
                Q(custom_id__icontains=query)
            )
            return exchange
        else:
            return exchange

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            serializer_data = serializer.data
            sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'])
            return self.get_paginated_response(sorted_serializer_data)

        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'])
        return Response(sorted_serializer_data)

class UpdateExchangeSatuanPendingView(RetrieveUpdateDestroyAPIView):
    serializer_class = ExchangeSatuanSerializer
    permission_classes = (permissions.IsAuthenticated, IsAdmin)
    
    lookup_field = "custom_id"
    queryset = ExchangeSatuan.objects.all()
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()

        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if serializer.is_valid():
            serializer_data = serializer.save()
            return Response({'message': "Detail Exchange Satuan Berhasil Diupdate"}, status=status.HTTP_200_OK)
        else:
            return Response({'message': "Detail Exchange Satuan Gagal Diupdate"}, status=status.HTTP_400_BAD_REQUEST)