from pyexpat import model
from exchange.models import ExchangePaketan, ExchangePenjemputan, ExchangeSatuan, PenjemputanWarung
from kabar.models import Kabar
from points.models import WithdrawPoints
from rest_framework import serializers
from location.models import Location
from authentication.models import Pengguna, Warung
# from .models import ExchangePaketan, ExchangePenjemputan, Transaction, ExchangeSatuan, PenjemputanWarung
from django.db.models import Sum

class WarungSerializer(serializers.ModelSerializer):
    longitude = serializers.SerializerMethodField('get_longitude')
    latitude = serializers.SerializerMethodField('get_latitude')
    email = serializers.SerializerMethodField('get_email')
    
    def get_longitude(self, warung):
        try:
            location = Location.objects.get(warung=warung)
            if location:
                return location.longitude
        except:
            return None
    
    def get_latitude(self, warung):
        try:
            location = Location.objects.get(warung=warung)
            if location:
                return location.latitude
        except:
            return None
    
    def get_email(self, warung):
        user = warung.user.email
        return user
    
    class Meta:
        model = Warung
        ordering = ['custom_id']
        fields = ["user","email", "custom_id", "name", "phone", "alamat","longitude","latitude", "city", "qr_code", "jumlah_satuan_pending", "jumlah_paketan_pending", "kapasitas", "is_ready", "is_confirmed"]
        
class PenggunaSerializer(serializers.ModelSerializer):
    email = serializers.SerializerMethodField('get_email')
    
    def get_email(self, pengguna):
        user = pengguna.user.email
        return user
        
    class Meta:
        model = Pengguna
        ordering = ['user']
        fields = ["user","email", "name", "phone", "city", "profile_img", "jumlahPenukaran", "pencapaian", "poin"]



class ExchangePaketanSerializer(serializers.ModelSerializer):
    pengguna_name = serializers.SerializerMethodField('get_pengguna_name')
    warung_name = serializers.SerializerMethodField('get_warung_name')
    
    def get_warung_name(self, exchange):
        warung = exchange.warung.name
        return warung
    
    def get_pengguna_name(self, exchange):
        pengguna = exchange.pengguna.name
        return pengguna
    
    class Meta:
        model = ExchangePaketan
        ordering = ['custom_id']
        fields = ["custom_id", "pengguna_name", "warung_name","botol_besar", "botol_sedang", "transaksi_warung", "botol_kecil", "gelas_besar", "gelas_kecil", "total_poin", "total_kemasan", "date", "is_confirmed", "is_picked_up", "is_created"]

class ExchangeSatuanSerializer(serializers.ModelSerializer):
    pengguna_name = serializers.SerializerMethodField('get_pengguna_name')
    warung_name = serializers.SerializerMethodField('get_warung_name')
    
    def get_warung_name(self, exchange):
        warung = exchange.warung.name
        return warung
    
    def get_pengguna_name(self, exchange):
        pengguna = exchange.pengguna.name
        return pengguna
    
    class Meta:
        model = ExchangePaketan
        ordering = ['custom_id']
        fields = ["custom_id", "pengguna_name", "warung_name","botol_besar", "botol_sedang", "botol_kecil", "gelas_besar", "gelas_kecil", "total_poin", "total_kemasan", "date", "is_confirmed", "is_picked_up", "is_created"]

class ExchangePenjemputanSerializer(serializers.ModelSerializer):
    pengguna_name = serializers.SerializerMethodField('get_pengguna_name')
    alamat = serializers.SerializerMethodField('get_alamat')

    def get_pengguna_name(self, exchange):
        pengguna = exchange.pengguna.name
        return pengguna
    
    def get_alamat(self, exchange):
        alamat = exchange.alamat.alamat
        return alamat
    
    class Meta:
        model = ExchangePenjemputan
        ordering = ['custom_id']
        fields = ["custom_id", "pengguna_name","alamat", "botol", "gelas","date", "is_confirmed"]
        

class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = ["warung", "latitude", "longitude", "address"]
        
class WithdrawSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField('get_name')
    user_type = serializers.SerializerMethodField('get_user_type')
    
    def get_name(self, withdraw):
        if withdraw.warung:
            return withdraw.warung.name
        else:
            return withdraw.pengguna.name
    
    def get_user_type(self, withdraw):
        if withdraw.warung:
            return "warung"
        else:
            return "pengguna"
    
    class Meta:
        model = WithdrawPoints
        fields = ["custom_id","user_type", "name", "total_poin", "pending_poin","phone","method", "is_confirmed"]
        
class KabarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kabar
        fields = ['id',"title", "date", "text", "thumbnail"]
        
class PenjemputanWarungSerializer(serializers.ModelSerializer):
    warung = serializers.SerializerMethodField('get_warung')
    total_satuan = serializers.SerializerMethodField('get_satuan')
    total_paketan = serializers.SerializerMethodField('get_paketan')
    total_botol_besar = serializers.SerializerMethodField('get_botol_besar')
    total_botol_sedang = serializers.SerializerMethodField('get_botol_sedang')
    total_botol_kecil = serializers.SerializerMethodField('get_botol_kecil')
    total_gelas_besar = serializers.SerializerMethodField('get_gelas_besar')
    total_gelas_kecil = serializers.SerializerMethodField('get_gelas_kecil')
    
    def get_warung(self, penjemputan):
        return penjemputan.warung.name
    
    def get_satuan(self, penjemputan):
        return len(ExchangeSatuan.objects.filter(penjemputan_warung=penjemputan))
    
    def get_paketan(self, penjemputan):
        return len(ExchangePaketan.objects.filter(penjemputan_warung=penjemputan))
    
    def get_botol_besar(self, penjemputan):
        return ExchangeSatuan.objects.filter(penjemputan_warung=penjemputan).aggregate(Sum('botol_besar'))['botol_besar__sum']
    
    def get_botol_sedang(self, penjemputan):
        return ExchangeSatuan.objects.filter(penjemputan_warung=penjemputan).aggregate(Sum('botol_sedang'))['botol_sedang__sum']
    
    def get_botol_kecil(self, penjemputan):
        return ExchangeSatuan.objects.filter(penjemputan_warung=penjemputan).aggregate(Sum('botol_kecil'))['botol_kecil__sum']
    
    def get_gelas_besar(self, penjemputan):
        return ExchangeSatuan.objects.filter(penjemputan_warung=penjemputan).aggregate(Sum('gelas_besar'))['gelas_besar__sum']
    
    def get_gelas_kecil(self, penjemputan):
        return ExchangeSatuan.objects.filter(penjemputan_warung=penjemputan).aggregate(Sum('gelas_kecil'))['gelas_kecil__sum']
    
    class Meta:
        model = PenjemputanWarung
        fields = ['custom_id',"warung","total_satuan", "total_paketan", "total_botol_besar", "total_botol_sedang", "total_botol_kecil", "total_gelas_besar", "total_gelas_kecil", "date", "status"]