from django.shortcuts import render

from http.client import NOT_FOUND
from django.shortcuts import render
import os
from authentication.models import Alamat, User, Pengguna, Warung
from exchange.models import ExchangePenjemputan, PenjemputanWarung, Transaction, ExchangeSatuan, ExchangePaketan
from exchange.permissions import IsPengguna, IsWarung
from points.models import TransferPoints, WithdrawPoints
from rest_framework.views import APIView
from rest_framework import generics, status, views, permissions, viewsets
from rest_framework.generics import ListAPIView, RetrieveAPIView, UpdateAPIView, CreateAPIView
from .serializers import ExchangeSatuanSerializer, ExchangePaketanSerializer, PenjemputanWarungSerializer, TransactionSerializer, ExchangePenjemputanSerializer, ExchangeSatuanPendingSerializer

from django.http import HttpResponsePermanentRedirect
from achievement.models import Achievement
from rest_framework.response import Response
from django.urls import reverse
import jwt
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
import pyotp
from rest_framework.response import Response
from rest_framework.views import APIView
import datetime
import calendar



class ExchangeSatuanView(CreateAPIView):
    serializer_class = ExchangeSatuanSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        pengguna = Pengguna.objects.get(user=self.request.user)
        transaksi = Transaction.objects.create(type="Satuan", pengguna=pengguna)
        warung = Warung.objects.get(custom_id=self.request.data["id_warung"])
        
        serializer_data =  serializer.save(pengguna=pengguna, transaksi= transaksi, warung=warung)
        transaksi_data = serializer.data
        transaksi.total_poin = transaksi_data["total_poin"]
        transaksi.save()

        return serializer_data

class UpdateExchangeSatuanView(UpdateAPIView):
    serializer_class = ExchangeSatuanSerializer
    permission_classes = (permissions.IsAuthenticated, IsWarung)
    lookup_field = "custom_id"
    queryset = ExchangeSatuan.objects.all()
    
    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        
        pengguna = instance.pengguna
        serializer = self.get_serializer(instance,data=request.data,partial=True)
        if not instance.is_confirmed:
            if serializer.is_valid():
                serializer_data = serializer.save()
                achievement = Achievement.objects.get(pengguna=pengguna)
                achievement.check_status()
                return Response({'message': "Exchange Satuan Berhasil Dikonfirmasi"}, status=status.HTTP_200_OK)
        else:
            return Response({'message': "Exchange sudah pernah dikonfirmasi"}, status=status.HTTP_400_BAD_REQUEST)


class ExchangePaketanView(CreateAPIView):
    serializer_class = ExchangePaketanSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        pengguna = Pengguna.objects.get(user=self.request.user)
        transaksi = Transaction.objects.create(type="Paketan", pengguna=pengguna)
        custom_id = self.request.data["custom_id"]
        warung_id = custom_id[:7]
        print(warung_id)
        warung = Warung.objects.get(custom_id=warung_id)
        warung.jumlah_paketan_pending += 1
        warung.save()

        serializer_data =  serializer.save(custom_id = custom_id,pengguna=pengguna, transaksi= transaksi, warung=warung)

        return serializer_data
    
class TransactionListView(ListAPIView):
    serializer_class = TransactionSerializer
    permission_classes = (permissions.IsAuthenticated,)
    
    def get_queryset(self):
        user = self.request.user
        if user.is_pengguna: 
            pengguna = Pengguna.objects.get(user=user)
            queryset = Transaction.objects.filter(pengguna=pengguna)
        if user.is_warung:
            warung = Warung.objects.get(user=user)
            queryset = Transaction.objects.filter(warung=warung)
        return queryset
    
    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            serializer_data = serializer.data
            sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'], reverse=True)
            return self.get_paginated_response(sorted_serializer_data)

        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'], reverse=True)
        return Response(sorted_serializer_data)
    
class PendingExchangeSatuanListView(ListAPIView):
    serializer_class = ExchangeSatuanPendingSerializer
    permission_classes = (permissions.IsAuthenticated,IsWarung)
    
    def get_queryset(self):
        user = self.request.user
        if user.is_warung:
            warung = Warung.objects.get(user=user)
            queryset = ExchangeSatuan.objects.filter(warung=warung, is_confirmed = False)
        return queryset
    
    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            serializer_data = serializer.data
            sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'], reverse=True)
            return self.get_paginated_response(sorted_serializer_data)

        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'], reverse=True)
        return Response(sorted_serializer_data)
    
class PenjemputanWarungView(CreateAPIView):
    serializer_class = PenjemputanWarungSerializer
    permission_classes = (permissions.IsAuthenticated,IsWarung)

    def perform_create(self, serializer):
        warung = Warung.objects.get(user=self.request.user)
        serializer_data =  serializer.save(warung=warung)

        penjemputan = serializer.data
        penjemputan_warung = PenjemputanWarung.objects.get(custom_id=penjemputan["custom_id"]) 
        
        exchange_satuan = ExchangeSatuan.objects.filter(warung=warung, is_picked_up=False)

        exchange_paketan = ExchangePaketan.objects.filter(warung=warung, is_picked_up=False)

        for satuan in exchange_satuan:
            satuan.penjemputan_warung = penjemputan_warung
            satuan.save_penjemputan_warung()
        
        for paketan in exchange_paketan:
            paketan.penjemputan_warung = penjemputan_warung
            paketan.save()
        
        warung.is_ready = False
        warung.save()
        return serializer_data        

class ExchangePenjemputanView(CreateAPIView):
    serializer_class = ExchangePenjemputanSerializer
    permission_classes = (permissions.IsAuthenticated,IsPengguna)

    def perform_create(self, serializer):
        pengguna = Pengguna.objects.get(user=self.request.user)
        transaksi = Transaction.objects.create(type="Penjemputan", pengguna=pengguna)
        
        alamat_id = self.request.data["alamat_id"]
        alamat = Alamat.objects.get(id=alamat_id)
        serializer_data =  serializer.save(alamat=alamat,pengguna=pengguna, transaksi= transaksi)
        
        return serializer_data

class ComissionAndStatisticsWarungView(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, IsWarung)
    def get(self, request):
        warung = Warung.objects.get(user = self.request.user)
        exchange_satuan = ExchangeSatuan.objects.filter(warung=warung, is_picked_up=False)
        
        commission = 0
        for exchange in exchange_satuan:
            commission += (exchange.total_poin/10)
            
        queryset = ExchangeSatuan.objects.filter(warung=warung, is_confirmed = False)
        print(commission)
        
        pending = PenjemputanWarung.objects.filter(warung=warung, status="")
        response = {
            'message': 'Komisi Warung Ditemukan',
            'commission': commission,
            'pending_exchange_satuan': len(queryset),
            'is_ready' : warung.is_ready
        }
        return Response(response)


class DetailTransactionView(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request, custom_id):
        user = self.request.user
        
        try:
            transaksi = Transaction.objects.get(custom_id =custom_id)    
            if transaksi.type == "Satuan":
                if user.is_warung:
                    exchange_satuan = ExchangeSatuan.objects.get(transaksi_warung = transaksi)
                if user.is_pengguna:
                    exchange_satuan = ExchangeSatuan.objects.get(transaksi = transaksi)
                status_code = status.HTTP_200_OK
                response = {
                    'message': 'Detail Exchange Satuan Ditemukan',
                    'data': [{
                        'transaksi': {
                            'custom_id' : transaksi.custom_id,
                            'type' : transaksi.type,
                            'date' : transaksi.date,
                            'status' : transaksi.status
                        },
                        'exchange_satuan': {
                            'custom_id': exchange_satuan.custom_id,
                            'warung' : exchange_satuan.warung.name,
                            'pengguna' : exchange_satuan.pengguna.name,
                            'botol_besar':exchange_satuan.botol_besar,
                            'botol_sedang':exchange_satuan.botol_sedang,
                            'botol_kecil':exchange_satuan.botol_kecil,
                            'gelas_besar':exchange_satuan.gelas_besar,
                            'gelas_kecil':exchange_satuan.gelas_kecil,
                            'date': exchange_satuan.date,
                            'is_confirmed': exchange_satuan.is_confirmed,
                            'total_poin' : exchange_satuan.total_poin
                        }
                    }]
                }
                return Response(response)
            
            if transaksi.type == "Paketan":
                if user.is_warung:
                    exchange_paketan = ExchangePaketan.objects.get(transaksi_warung = transaksi)
                if user.is_pengguna:
                    exchange_paketan = ExchangePaketan.objects.get(transaksi = transaksi)
                
                status_code = status.HTTP_200_OK
                response = {
                    'message': 'Detail Exchange Paketan Ditemukan',
                    'data': [{
                        'transaksi': {
                            'custom_id' : transaksi.custom_id,
                            'type' : transaksi.type,
                            'date' : transaksi.date,
                            'status' : transaksi.status
                        },
                        'exchange_paketan': {
                            'custom_id': exchange_paketan.custom_id,
                            'warung' : exchange_paketan.warung.name,
                            'pengguna' : exchange_paketan.pengguna.name,
                            'botol_besar':exchange_paketan.botol_besar,
                            'botol_sedang':exchange_paketan.botol_sedang,
                            'botol_kecil':exchange_paketan.botol_kecil,
                            'gelas_besar':exchange_paketan.gelas_besar,
                            'gelas_kecil':exchange_paketan.gelas_kecil,
                            'date': exchange_paketan.date,
                            'is_confirmed': exchange_paketan.is_confirmed,
                            'total_poin' : exchange_paketan.total_poin
                        }
                    }]
                }
                return Response(response)

            if transaksi.type == "Penjemputan":
                exchange_penjemputan = ExchangePenjemputan.objects.get(transaksi = transaksi)
                status_code = status.HTTP_200_OK
                response = {
                    'message': 'Detail Exchange Penjemputan Ditemukan',
                    'data': [{
                        'transaksi': {
                            'custom_id' : transaksi.custom_id,
                            'type' : transaksi.type,
                            'date' : transaksi.date,
                            'status' : transaksi.status
                        },
                        'exchange_penjemeputan': {
                            'custom_id': exchange_penjemputan.custom_id,
                            'alamat' : exchange_penjemputan.alamat.alamat,
                            'botol':exchange_penjemputan.botol,
                            'gelas':exchange_penjemputan.gelas,
                            'date': exchange_penjemputan.date,
                            'is_confirmed': exchange_penjemputan.is_confirmed,
                            'total_poin' : transaksi.total_poin
                        }
                    }]
                }
                return Response(response)
            
            if transaksi.type == "Transfer":
                transfer = TransferPoints.objects.get(transaksi = transaksi)
                status_code = status.HTTP_200_OK
                response = {
                    'message': 'Detail Transfer Point Ditemukan',
                    'data': [{
                        'transaksi': {
                            'custom_id' : transaksi.custom_id,
                            'type' : transaksi.type,
                            'date' : transaksi.date,
                            'status' : transaksi.status
                        },
                        'transfer': {
                            'custom_id': transfer.custom_id,
                            'pengguna' : transfer.pengguna.name,
                            'warung' : transfer.warung.name,
                            'total_poin':transfer.total_poin,
                            'is_confirmed': transfer.is_confirmed
                        }
                    }]
                }
                return Response(response)
            
            if transaksi.type == "Withdraw":
                withdraw = WithdrawPoints.objects.get(transaksi = transaksi)
                status_code = status.HTTP_200_OK
                response = {
                    'message': 'Detail Withdraw POint Ditemukan',
                    'data': [{
                        'transaksi': {
                            'custom_id' : transaksi.custom_id,
                            'type' : transaksi.type,
                            'date' : transaksi.date,
                            'status' : transaksi.status
                        },
                        'withdraw': {
                            'custom_id': withdraw.custom_id,
                            'total_poin':withdraw.total_poin,
                            'phone': withdraw.phone,
                            'method': withdraw.method,
                            'is_confirmed': withdraw.is_confirmed
                        }
                    }]
                }
                return Response(response)
            
            if transaksi.type == "Reward":
                status_code = status.HTTP_200_OK
                response = {
                    'message': 'Detail Reward Ditemukan',
                    'data': [{
                        'transaksi': {
                            'custom_id' : transaksi.custom_id,
                            'type' : transaksi.type,
                            'date' : transaksi.date,
                            'status' : transaksi.status
                        },
                        'reward': {
                            'total_poin': transaksi.total_poin,
                        }
                    }]
                }
                return Response(response)
            
        except Exception as e:
            status_code = status.HTTP_400_BAD_REQUEST
            response = {
                'status code': status.HTTP_400_BAD_REQUEST,
                'message': 'Transaksi tidak dapat ditemukan',
                'error': str(e)
            }
            return Response(response)

class DetailExchangeSatuanView(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, IsPengguna)
    def get(self, request, custom_id):
        warung = Pengguna.objects.get(user = self.request.user)
        
        transaksi = Transaction.objects.get(custom_id =custom_id)    
        exchange_satuan = ExchangeSatuan.objects.get(transaksi = transaksi)
        status_code = status.HTTP_200_OK
        response = {
            'message': 'Detail Exchange Satuan Ditemukan',
            'data': [{
                'transaksi': {
                    'custom_id' : transaksi.custom_id,
                    'type' : transaksi.type,
                    'date' : transaksi.date,
                    'status' : transaksi.status
                },
                'exchange_satuan': {
                    'custom_id': exchange_satuan.custom_id,
                    'warung' : exchange_satuan.warung.name,
                    'botol_besar':exchange_satuan.botol_besar,
                    'botol_sedang':exchange_satuan.botol_sedang,
                    'botol_kecil':exchange_satuan.botol_kecil,
                    'gelas_besar':exchange_satuan.gelas_besar,
                    'gelas_kecil':exchange_satuan.gelas_kecil,
                    'date': exchange_satuan.date,
                    'is_confirmed': exchange_satuan.is_confirmed,
                    'total_poin' : exchange_satuan.total_poin
                }
            }]
        }
        return Response(response)
    
class StatistikWarungView(generics.GenericAPIView):
    permission_classes = (permissions.IsAuthenticated, IsWarung)

    def get(self, request):
        warung = Warung.objects.get(user = self.request.user)
    
        date = datetime.datetime.today()
        week = date.strftime("%V")
        month = date.strftime("%m")
        
        satuan_week = ExchangeSatuan.objects.filter(warung=warung, date__week=week)
        paketan_week = ExchangePaketan.objects.filter(warung=warung, date__week=week)
        
        
        exchange_count_day = [0,0,0,0,0,0,0]
        
        paketan_counts_week = 0
        commission_week = 0
        botol_besar_week = 0
        botol_sedang_week= 0
        botol_kecil_week = 0
        gelas_besar_week = 0
        gelas_kecil_week = 0
        
        for satuan in satuan_week:
            if satuan.date.strftime("%A").lower() == "monday":
                exchange_count_day[0] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                commission_week += satuan.total_poin
            elif satuan.date.strftime("%A").lower() == "tuesday":
                exchange_count_day[1] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                commission_week += satuan.total_poin
            elif satuan.date.strftime("%A").lower() == "wednseday":
                exchange_count_day[2] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                commission_week += satuan.total_poin
            elif satuan.date.strftime("%A").lower() == "thursday":
                exchange_count_day[3] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                commission_week += satuan.total_poin
            elif satuan.date.strftime("%A").lower() == "friday":
                exchange_count_day[4] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                commission_week += satuan.total_poin
            elif satuan.date.strftime("%A").lower() == "saturday":
                exchange_count_day[5] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                commission_week += satuan.total_poin
            else:
                exchange_count_day[6] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                commission_week += satuan.total_poin
                
            botol_besar_week += satuan.botol_besar
            botol_sedang_week += satuan.botol_sedang
            botol_kecil_week += satuan.botol_kecil
            gelas_besar_week += satuan.gelas_besar
            gelas_kecil_week += satuan.gelas_kecil
                
        satuan_counts_week = sum(exchange_count_day)
        
        for satuan in paketan_week:
            if satuan.date.strftime("%A").lower() == "monday":
                exchange_count_day[0] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil 
                commission_week += satuan.total_poin
            elif satuan.date.strftime("%A").lower() == "tuesday":
                exchange_count_day[1] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                commission_week += satuan.total_poin
            elif satuan.date.strftime("%A").lower() == "wednseday":
                exchange_count_day[2] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                commission_week += satuan.total_poin
            elif satuan.date.strftime("%A").lower() == "thursday":
                exchange_count_day[3] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                commission_week += satuan.total_poin
            elif satuan.date.strftime("%A").lower() == "friday":
                exchange_count_day[4] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                commission_week += satuan.total_poin
            elif satuan.date.strftime("%A").lower() == "saturday":
                exchange_count_day[5] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                commission_week += satuan.total_poin
            else:
                exchange_count_day[6] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                commission_week += satuan.total_poin
                
            botol_besar_week += satuan.botol_besar
            botol_sedang_week += satuan.botol_sedang
            botol_kecil_week += satuan.botol_kecil
            gelas_besar_week += satuan.gelas_besar
            gelas_kecil_week += satuan.gelas_kecil
            
        paketan_counts_week = sum(exchange_count_day) - satuan_counts_week
        
        transfer_week = 0
        transfer_transaction = Transaction.objects.filter(warung=warung, date__week=week, type="Transfer")
        if len(transfer_transaction) > 0:
            for transaksi in transfer_transaction:
                transfer = TransferPoints.objects.get(transaksi=transaksi)
                if transfer.is_confirmed:
                    transfer_week += transfer.total_poin
        
        #Monthly
        now = datetime.datetime.now()
        satuan_month = ExchangeSatuan.objects.filter(warung=warung, date__month=now.month)
        paketan_month = ExchangePaketan.objects.filter(warung=warung, date__month=now.month)
        
        days_of_month = calendar.monthrange(now.year, now.month)[1]
        exchange_month_count_day = []
        satuan_counts_month = 0
        commission_month = 0
        
        botol_besar_month = 0
        botol_sedang_month= 0
        botol_kecil_month = 0
        gelas_besar_month = 0
        gelas_kecil_month = 0
        for i in range(days_of_month):
            exchange_month_count_day.append(0)
            satuan_exchange = satuan_month.filter(date__day=i+1)
            if len(satuan_exchange)>0:
                for satuan in satuan_exchange:
                    exchange_month_count_day[i] += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
                    botol_besar_month += satuan.botol_besar
                    botol_sedang_month += satuan.botol_sedang
                    botol_kecil_month += satuan.gelas_besar
                    gelas_besar_month += satuan.botol_besar
                    gelas_kecil_month += satuan.gelas_kecil
                    
                    commission_month += satuan.total_poin
        
        satuan_counts_month = sum(exchange_month_count_day)

        for i in range(days_of_month):
            paketan_exchange = paketan_month.filter(date__day=i+1)
            if len(paketan_exchange)>0:
                for paketan in paketan_exchange:
                    exchange_month_count_day[i] += paketan.botol_besar + paketan.botol_sedang + paketan.botol_kecil + paketan.gelas_besar + paketan.gelas_kecil

                    botol_besar_month += paketan.botol_besar
                    botol_sedang_month += paketan.botol_sedang
                    botol_kecil_month += paketan.gelas_besar
                    gelas_besar_month += paketan.botol_besar
                    gelas_kecil_month += paketan.gelas_kecil
                    commission_month += paketan.total_poin
                    
        paketan_counts_month = sum(exchange_month_count_day) - satuan_counts_month
        
        transfer_month = 0
        transfer_transaction = Transaction.objects.filter(warung=warung, date__month=month, type="Transfer")
        if len(transfer_transaction) > 0:
            for transaksi in transfer_transaction:
                transfer = TransferPoints.objects.get(transaksi=transaksi)
                if transfer.is_confirmed:
                    transfer_month += transfer.total_poin
        
        response = {
                'success': 'true',
                'status code': status.HTTP_200_OK,
                'message': 'Statistik Ditemukan',
                'graph_week': exchange_count_day,
                'graph_month': exchange_month_count_day,
                'details_week':[{
                    'satuan_counts': satuan_counts_week,
                    'paketan_counts':paketan_counts_week,
                    'commission':commission_week,
                    'transfer':transfer_week,
                    'botol_besar':botol_besar_week,
                    'botol_sedang':botol_sedang_week,
                    'botol_kecil':botol_kecil_week,
                    'gelas_besar':gelas_besar_week,
                    'gelas_kecil':gelas_kecil_week,
                }],
                'details_month':[{
                    'days': days_of_month,
                    'satuan_counts':satuan_counts_month,
                    'paketan_counts':paketan_counts_month,
                    'commission':commission_month,
                    'transfer':transfer_month,
                    'botol_besar':botol_besar_month,
                    'botol_sedang':botol_sedang_month,
                    'botol_kecil':botol_kecil_month,
                    'gelas_besar':gelas_besar_month,
                    'gelas_kecil':gelas_kecil_month,
                }]
        }
        return Response(response)
    
class PenjemputanWarungListView(ListAPIView):
    serializer_class = PenjemputanWarungSerializer
    permission_classes = (permissions.IsAuthenticated,IsWarung)
    
    def get_queryset(self):
        user = self.request.user
        if user.is_warung:
            warung = Warung.objects.get(user=user)
            queryset = PenjemputanWarung.objects.filter(warung=warung)
        return queryset
    
    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            serializer_data = serializer.data
            sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'], reverse=True)
            return self.get_paginated_response(sorted_serializer_data)

        serializer = self.get_serializer(queryset, many=True)
        serializer_data = serializer.data
        sorted_serializer_data = sorted(serializer_data, key=lambda x: x['date'], reverse=True)
        return Response(sorted_serializer_data)