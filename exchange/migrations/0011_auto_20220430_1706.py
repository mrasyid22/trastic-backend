# Generated by Django 3.1.7 on 2022-04-30 17:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exchange', '0010_auto_20220425_0148'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transaction',
            name='type',
            field=models.CharField(blank=True, choices=[('Satuan', 'Satuan'), ('Paketan', 'Paketan'), ('Penjemputan', 'Penjemputan'), ('Transfer', 'Transfer'), ('Withdraw', 'Withdraw'), ('Reward', 'Reward')], default='Satuan', max_length=30),
        ),
    ]
