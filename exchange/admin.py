from django.contrib import admin

from .models import ExchangePaketan, ExchangePenjemputan, Transaction, ExchangeSatuan, PenjemputanWarung
admin.site.register(Transaction)
admin.site.register(ExchangeSatuan)
admin.site.register(ExchangePaketan)
admin.site.register(ExchangePenjemputan)
admin.site.register(PenjemputanWarung)