from django.urls import path, re_path
from .views import DetailTransactionView, ExchangeSatuanView, TransactionListView, UpdateExchangeSatuanView, ExchangePaketanView, ExchangePenjemputanView, ComissionAndStatisticsWarungView, PendingExchangeSatuanListView, PenjemputanWarungView, StatistikWarungView, PenjemputanWarungListView

app_name = "exchange"
urlpatterns = [
    path('exchange-satuan/', ExchangeSatuanView.as_view(),name='exchange-satuan'),
    re_path('^exchange-satuan/update/(?P<custom_id>\w+)$',UpdateExchangeSatuanView.as_view(), name='update exchange satuan'),
    path('exchange-paketan/', ExchangePaketanView.as_view(),name='exchange-paketan'),
    path('exchange-penjemputan/', ExchangePenjemputanView.as_view(),name='exchange-penjemputan'),
    path('transactions/', TransactionListView.as_view(), name="transactions"),
    path('transaction/detail/<custom_id>', DetailTransactionView.as_view(), name="detail transactions"),
    path('warung-statistics/', ComissionAndStatisticsWarungView.as_view(),name='warung-commission'),
    path('pending-exchange-satuan/', PendingExchangeSatuanListView.as_view(),name='pending-satuan'),
    path('penjemputan-warung/', PenjemputanWarungView.as_view(),name='penjemputan-warung'),
    path('statistik-warung/', StatistikWarungView.as_view(),name='statistik-warung'),
    path('list-penjemputan-warung/', PenjemputanWarungListView.as_view(),name='list-penjemputan-warung')
]