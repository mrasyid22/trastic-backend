from pyexpat import model
from rest_framework import serializers
from .models import ExchangePaketan, ExchangePenjemputan, Transaction, ExchangeSatuan, PenjemputanWarung


class ExchangeSatuanSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExchangeSatuan
        fields = ["pengguna_id", "transaksi_id", "warung_id", "botol_besar", "botol_sedang", "botol_kecil", "gelas_besar", "gelas_kecil", "date", "total_poin","is_confirmed"]
        
class ExchangePaketanSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExchangePaketan
        fields = ["custom_id","pengguna_id", "transaksi_id", "warung_id", "botol_besar", "botol_sedang", "botol_kecil", "gelas_besar", "gelas_kecil", "date", "total_poin","is_confirmed"]
        
class ExchangePenjemputanSerializer(serializers.ModelSerializer):

    class Meta:
        model = ExchangePenjemputan
        fields = ["pengguna_id","alamat_id", "transaksi_id","botol", "gelas", "date","is_confirmed"]
        
class PenjemputanWarungSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField('get_status')
    custom_id = serializers.SerializerMethodField('get_custom_id')
    total_poin = serializers.SerializerMethodField('get_total_poin')
    satuan = serializers.SerializerMethodField('get_satuan')
    paketan = serializers.SerializerMethodField('get_paketan')
    
    def get_status(self, penjemputan_warung):
        name = penjemputan_warung.status
        return name

    def get_custom_id(self, penjemputan_warung):
        custom_id = penjemputan_warung.custom_id
        return custom_id
    
    def get_total_poin(self, penjemputan_warung):
        total_poin = 0
        exchange_satuan = ExchangeSatuan.objects.filter(penjemputan_warung=penjemputan_warung)
        for satuan in exchange_satuan:
            total_poin += satuan.total_poin
        
        exchange_paketan = ExchangePaketan.objects.filter(penjemputan_warung=penjemputan_warung)
        for paketan in exchange_paketan:
            total_poin += paketan.total_poin
            
        return float(total_poin)*0.1
    
    def get_satuan(self, penjemputan_warung):
        satuan_total = 0
        exchange_satuan = ExchangeSatuan.objects.filter(penjemputan_warung=penjemputan_warung)
        for satuan in exchange_satuan:
            satuan_total += satuan.botol_besar + satuan.botol_sedang + satuan.botol_kecil + satuan.gelas_besar + satuan.gelas_kecil
        return satuan_total
    
    def get_paketan(self, penjemputan_warung):
        paketan_total = 0
        exchange_paketan = ExchangePaketan.objects.filter(penjemputan_warung=penjemputan_warung)
        for paketan in exchange_paketan:
            paketan_total += paketan.botol_besar + paketan.botol_sedang + paketan.botol_kecil + paketan.gelas_besar + paketan.gelas_kecil
        return paketan_total
        
        
    class Meta:
        model = PenjemputanWarung
        fields = ["warung","custom_id", "date", "status", "total_poin", "satuan", "paketan"]


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ["custom_id", "total_poin", "status", "type", "date"]
        
        
class ExchangeSatuanPendingSerializer(serializers.ModelSerializer):
    name_pengguna = serializers.SerializerMethodField('get_pengguna_warung')
    
    def get_pengguna_warung(self, exchange_satuan):
        name = exchange_satuan.pengguna.name
        return name
    
    class Meta:
        model = ExchangeSatuan
        fields = ["name_pengguna", "custom_id","transaksi_id", "warung", "botol_besar", "botol_sedang", "botol_kecil", "gelas_besar", "gelas_kecil", "date", "total_poin","is_confirmed"]
        
    