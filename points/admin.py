from django.contrib import admin

from .models import TransferPoints, WithdrawPoints
admin.site.register(TransferPoints)
admin.site.register(WithdrawPoints)

